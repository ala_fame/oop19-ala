package ala.models;

public interface ParadiseEnemiesAbilitiesModel {
    /**
      * make paradise enemies move right and left up to a maximum distance.
      * 
      */
    void moveEnemy();
}
