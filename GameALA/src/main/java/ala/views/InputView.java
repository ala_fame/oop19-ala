package ala.views;

public interface InputView {
    /**
      * add listeners.
      * 
      */
     void addListeners();
 
    /**
      * remove listeners.
      * 
      */
     void removeListeners();
}
